import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { ItemComponent } from './item/item';
import { VotesComponent } from './votes/votes';
import { CommentBoxComponent } from './comment-box/comment-box';
import { CommentComponent } from './comment/comment';
import { NavTopComponent } from './nav-top/nav-top';
import { SourceFollowComponent } from './source-follow/source-follow';

@NgModule({
	declarations: [
		ItemComponent,
		VotesComponent,
		CommentBoxComponent,
		CommentComponent,
    	NavTopComponent,
    	SourceFollowComponent
	],
	imports: [IonicModule],
	exports: [
		ItemComponent,
		VotesComponent,
	    CommentBoxComponent,
	    CommentComponent,
    	NavTopComponent,
    	SourceFollowComponent
	]
})
export class ComponentsModule {}
