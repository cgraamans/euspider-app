import { Component, Input, Output, EventEmitter } from '@angular/core';
import { ModelDataProvider } from '../../providers/model-data/model-data';
import { ToastController } from 'ionic-angular';
import { ServiceSocketProvider } from '../../providers/service-socket/service-socket';
@Component({
  selector: 'votes',
  templateUrl: 'votes.html'
})
export class VotesComponent {

  @Input('vote') vote;
  @Input('votes') votes;
  @Input('type') type;
  @Input('id') id;

  public isLoggedIn:Boolean = false;

  constructor(public data:ModelDataProvider,public toastCtrl: ToastController, public socket:ServiceSocketProvider) {

  	this.data.getUserObservable().subscribe(val=> val ? this.isLoggedIn = true : this.isLoggedIn = false);

  }

  showToast($evt) {

  	$evt.stopPropagation();
    let toast = this.toastCtrl.create({
      message: 'You must login or register to vote',
      duration: 1250
    });
    toast.present();

  }

  submitVote($evt,dir){

  	$evt.stopPropagation();
  	let emit = {
  		vote:0
    };

  	if(!this.type) this.type = 'item';
  	emit[this.type] = this.id;

    	if(dir==='up'){

    		if(this.vote < 1) {

    			emit.vote = 1;

    		}

    	} else {

    		if(this.vote > -1){

    			emit.vote = -1

    		}

  	}
  	this.socket.emit('user-vote',emit);

  }

  score(vote){

    let copyVote = vote,
      divideBy = 1,
      designator = ''; 
    
    if(vote <1) copyVote = copyVote * -1;

    if(copyVote>1000) {
      divideBy = 100
      designator = 'k';
    };  

    if(copyVote>1000000) {
      divideBy = 100000;
      designator = 'm';
    }

    if(divideBy>1){
      vote = Math.round(vote/(divideBy))/10 + designator;
    }
    return vote;

  }

}
