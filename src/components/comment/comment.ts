import { Component, Input, Output, EventEmitter } from '@angular/core';

import { PopoverController, AlertController } from 'ionic-angular';
import { ServiceSocketProvider } from '../../providers/service-socket/service-socket';

import { ModelDataProvider } from '../../providers/model-data/model-data';

import { CommentPage } from '../../pages/comment/comment';
import { PopoverCommentBoxPage } from '../../pages/popover-comment-box/popover-comment-box';

import marked from 'marked';

@Component({
  selector: 'comment',
  templateUrl: 'comment.html'
})
export class CommentComponent {

	@Input('comment') comment;
	@Output() requestReplies = new EventEmitter<string>();

	public toggledComment:Boolean = false;
	constructor(private alertCtrl:AlertController, private data:ModelDataProvider,private popCtrl:PopoverController,private socket:ServiceSocketProvider) {
		

	}

	// ngOnInit() {

	// 	this.requestReplies.emit(this.comment.stub);

	// }

	toggleComment() {

		this.comment._toggle = !this.comment._toggle;

	}

	toggleReply(evt){

	    let popover = this.popCtrl.create(PopoverCommentBoxPage,{comment:this.comment.stub},{cssClass: 'comment-popover'});
	    popover.present({
	      ev: evt
	    });

	}

	toggleEdit(evt){

	    let popover = this.popCtrl.create(PopoverCommentBoxPage,{txt:this.comment.txt,edit:this.comment.stub},{cssClass: 'comment-popover'});
	    popover.present({
	      ev: evt
	    });

	}

	toggleSave(){

		let emit = {comment:this.comment.stub,save:1};
		if(this.comment.isSaved === 1){
			emit.save = 0;
		}
		this.socket.emit('user-save',emit);

	}

	toggleDelete(){


	    let alert = this.alertCtrl.create({
	      title: 'Are you sure?',
	      buttons: ['Ok','Cancel']
	    });

	    alert.present();

		// let emit = {delete:this.comment.stub};
		// this.socket.emit('comment-action',emit);

	}

	remark(txt) {

		return marked(txt.toString()); 

	}

	dt(unixtime){

		let elapsed = Math.round((new Date()).getTime()/1000) - unixtime;

		if(elapsed < 0){

			return '0 s';

		}

		if(elapsed < 60) {

			return elapsed+' s';

		}
		if(elapsed < 3600) {

			return Math.round((elapsed/60)*10)/10+' m';

		}

		if(elapsed < 86400) {

			return Math.round((elapsed/3600)*10)/10+' h';

		}

		if(elapsed >= 86400) {

			return Math.round((elapsed/86400)*10)/10+' d';

		}

	}

	isLoggedIn() {

		return this.data.getUser()? true : false;

	}

	passReplies(evt){

		this.requestReplies.emit(evt);

	}

	getReplies(){

		this.requestReplies.emit(this.comment.stub);
		this.comment._toggle = !this.comment._toggle;
	}

}
