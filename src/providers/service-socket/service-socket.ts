import { Socket } from 'ng-socket-io';
import { Storage } from '@ionic/storage';
import { Injectable } from '@angular/core';
import { AlertController, Events } from 'ionic-angular';
import { ServiceAuthProvider } from '../service-auth/service-auth';
import { ModelDataProvider } from '../model-data/model-data';

import 'rxjs/add/operator/map';

@Injectable()
export class ServiceSocketProvider {

  constructor(private events:Events, private alertCtrl: AlertController, private socket:Socket, private storage:Storage, public auth:ServiceAuthProvider, public data:ModelDataProvider) {

    socket.on('init',init=>{

      if(init.ok===true) {

        this.auth.token.next(init.secret);
        this.storage.ready().then(() => {

          this.storage.get('_user').then(val => {

            if(val){

              if((val.apiId) && (val.token)) {

                this.emit('auth',{apiId:val.apiId,token:val.token});

              }

            }

          });

        });

      }

    });

    socket.on('auth',authMsg=>{

      this.credentials(authMsg);

    });
    socket.on('auth-register',authMsg=>{

      this.credentials(authMsg);

    });
    
  }

  emit(toSocket,message){

    let usr:any = this.data.getUser();
    if((usr) && (typeof message === 'object')){
      message._user = {
        apiId:usr.apiId,
        token:usr.token
      };        
    }

    this.auth.encode(message,val=>{

      this.socket.emit(toSocket,val);

    });

  }

  on(fromSocket) {

    return this.socket
      .fromEvent(fromSocket)
      .map(msg=>this.auth.decode(msg));

  }

  private credentials(msg:any){

    let authMsg = this.auth.decode(msg);
    if(authMsg.ok === true) {

      if(authMsg.user){
      
        this.data.login(authMsg.user.apiId,authMsg.user.token,authMsg.user.auth,authMsg.user.name);  
        this.events.publish('item:refresh');

      }

    } else {

      this.data.logout();
      if(authMsg.m){

        if(authMsg.m.length>0){
          this.error(authMsg.m[0]);
        }

      }
      if(authMsg.e){

        this.error(authMsg.e);

      }

    }

  }

  private error(msg:string){

    let alert = this.alertCtrl.create({
      title: 'Error!',
      subTitle: msg,
      buttons: ['Ok']
    });

    alert.present();

  }

}