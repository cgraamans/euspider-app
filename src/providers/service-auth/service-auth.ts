import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import * as jwt from 'jsonwebtoken';

import 'rxjs/add/operator/takeWhile';

@Injectable()
export class ServiceAuthProvider {

  public token:BehaviorSubject<string> = new BehaviorSubject(null);

  constructor(){}

  encode(message,callback) {

    let val = this.token.getValue();
    if(!val){
      
      setTimeout(()=>{

        this.encode(message,callback);

      },1000);

    } else {

      callback(jwt.sign(message,val));

    }

  }

  decode(message) {

    let token = this.token.getValue();
    if(token){
      try {
        return jwt.verify(message,token);
      } catch(e) {
        return {ok:false,e:e,msg:message};
      }      
    } else {
      return {ok:false,e:'No valid token'};
    }
  
  }

}