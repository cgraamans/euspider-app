import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Storage } from '@ionic/storage';
import { MenuController } from 'ionic-angular';

@Injectable()
export class ModelDataProvider {

  //
  // Settings
  //

  assets = {

  	images:'https://api.euspider.eu/assets/' // image assets for avatar icons
  
  };

  settings = {

  	theme:'light-theme', // starting theme
  	language:'en', // starting language
    sourceType:null // starting sourceType

  };

  //
  // DO NOT EDIT BELOW
  //

  private theme: BehaviorSubject<String> = new BehaviorSubject('light-theme');
  private user:BehaviorSubject<Object> = new BehaviorSubject(null);
  private userLanguage:BehaviorSubject<String> = new BehaviorSubject('en'); 
  private sourceType:BehaviorSubject<String> = new BehaviorSubject(null);

  constructor(private storage:Storage){

    this.storage.ready().then(() => {

    	this.storage.get('_language').then(val => {

    		if(!val){

    			this.storage.set('_language',this.userLanguage.getValue());

    		} else {

    			this.userLanguage.next(val);

    		}

    	});

    	this.storage.get('_theme').then(val => {

    		if(!val){
          
          
            this.storage.set('_theme',this.theme.getValue());
          

    		} else {

    			this.theme.next(val);

    		}

    	});
      
    });
    

  }

  setLanguage(lang:string) {

    this.storage.ready().then(() => {

  	  this.storage.set('_language',lang);

    });
  	this.userLanguage.next(lang);

  }

  setSourceType(type:any) {

    this.sourceType.next(type);

  }

  getLanguage() {

    return this.userLanguage.getValue();

  }

  getSourceType(){

    return this.sourceType.getValue();

  }

  getSourceTypeString() {

    let rtn:any = null,
      sT = this.sourceType.getValue();

     if(sT){

       if(sT === 'N'){
         rtn = 'News';
       }
       if(sT === 'T'){
         rtn = 'ThinkTanks';
       }
       if(sT === 'O'){
         rtn = 'Official';
       }
       if(sT === 'NS'){
         rtn = 'News - Sports';
       }
       if(sT === 'NP'){
         rtn = 'News - Politics';
       }
       if(sT === 'NT'){
         rtn = 'News - SciTech';
       }
       
     }
     return rtn;

  }

  getSourceTypeObservable() {

    return this.sourceType.asObservable();

  }

  getLanguageObservable() {

    return this.userLanguage.asObservable();

  }

  login(apiId,token,auth,name){

  	this.user.next({
  		auth:auth,
  		apiId:apiId,
  		token:token,
  		name:name	
  	});

    this.storage.ready().then(() => {

      this.storage.set('_user',this.user.getValue());

    });

  }

  logout(){

    this.user.next(null);

    this.storage.ready().then(() => {

      this.storage.remove('_user');

    });

  }

  setActiveTheme(val) {

    this.storage.ready().then(() => {

      this.storage.set('_theme',val);

    });
    this.theme.next(val);

  }

  getActiveThemeObservable() {

    return this.theme.asObservable();

  }

  getActiveTheme() {

    return this.theme.getValue();

  }

  getUserObservable() {

    return this.user.asObservable();

  }

  getUser() {

    return this.user.getValue();

  }

}