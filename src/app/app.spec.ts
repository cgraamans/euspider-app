import {} from 'jasmine';
import { TestBed, ComponentFixture, async } from '@angular/core/testing';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { SocketIoModule, SocketIoConfig } from 'ng-socket-io';
const config: SocketIoConfig = { url: 'http://192.168.178.248:9002', options: {} };

import { MyApp } from './app.component';
import { ItemsPage } from '../pages/items/items';
import { ModelDataProvider } from '../providers/model-data/model-data';
import { ServiceAuthProvider } from '../providers/service-auth/service-auth';
import { ServiceSocketProvider } from '../providers/service-socket/service-socket';
 
let comp: MyApp;
let fixture: ComponentFixture<MyApp>;
 
describe('Component: Root Component', () => {
 
    beforeEach(async(() => {
 
        TestBed.configureTestingModule({
 
              declarations: [
                MyApp,
                ItemsPage
              ],
              imports: [
                BrowserModule,
                SocketIoModule.forRoot(config),
                IonicModule.forRoot(MyApp)
              ],
              providers: [
                StatusBar,
                SplashScreen,
                {provide: ErrorHandler, useClass: IonicErrorHandler},
                ModelDataProvider,
                ServiceAuthProvider,
                ServiceSocketProvider,
              ]
 
        }).compileComponents();
 
    }));
 
    beforeEach(() => {
 
        fixture = TestBed.createComponent(MyApp);
        comp    = fixture.componentInstance;
 
    });
 
    afterEach(() => {
        fixture.destroy();
        comp = null;
    });
 
    it('is created', () => {
 
        expect(fixture).toBeTruthy();
        expect(comp).toBeTruthy();
 
    });
 
    it('displays the items page to the user', () => {
        expect(comp['rootPage']).toBe(ItemsPage);
    });
 
});