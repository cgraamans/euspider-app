import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Refresher, PopoverController } from 'ionic-angular';

import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import 'rxjs/add/observable/combineLatest';

import { ModelDataProvider } from '../../providers/model-data/model-data';
import { ServiceSocketProvider } from '../../providers/service-socket/service-socket';

import { PopoverCommentBoxPage } from '../popover-comment-box/popover-comment-box';

@IonicPage()
@Component({
  selector: 'page-comments',
  templateUrl: 'comments.html',
})
export class CommentsPage {

  item:any = {};
  comments = [];
  assetsLocation = this.data.assets.images;
  
  category:BehaviorSubject<String> = new BehaviorSubject('likes');
  ready = false;
  crc = Math.random().toString(36).replace(/[^a-z]+/g, '');
  refresher:Refresher = null;
  infiniteScroller:any = null;
  commentBox:boolean = false;
  subscriptions = {
  	items:null,
  	comments:null,
  	userVote:null,
    userSave:null,
  	category:null,
    userComment:null,
  };
  replies = [];

  constructor(private popCtrl:PopoverController, public navCtrl: NavController, public navParams: NavParams, public data:ModelDataProvider, private socket:ServiceSocketProvider) {

    if(!navParams.get('item')) this.navCtrl.popToRoot();

      this.subscriptions.items = this.socket.on('items').subscribe(val=>{

    	if(val.ok === true) {

    		if(val._crc){

    			if(val._crc === this.crc) {

		    		this.item = val.items[0];
            this.item._details = false;
		    		this.ready = true;
    				this.socket.emit('comments',{
    					item:this.item.stub,
    					_crc:this.crc
    				});

    			}

    		}

    	}

    });

    this.subscriptions.comments = this.socket.on('comments').subscribe(val=>{

    	if(val.ok === true){

  			if(val._crc){

  				if(val._crc === this.crc){

  					if (this.refresher) {

  						this.comments = [];
  						this.refresher.complete();
  						this.refresher = null;

  					}

  					if(this.infiniteScroller) {

  						this.infiniteScroller.complete();
  						this.infiniteScroller = null;

  					}

  					if(val.comments.length>0){

              if(val.parent){
                  
                for(let newItem of val.comments){

                  newItem.replies = [];
                  newItem._toggle = true;
                  this.setReplies(val.parent,newItem);

                }
                
              } else {

                for(let newItem of val.comments){
           
                  newItem._toggle = false
                  newItem.replies = [];

                  if(this.comments.length>0){

                    let found = this.comments.findIndex(x => x.stub === newItem.stub);
                    if(found < 0){
                      this.comments.push(newItem);
                    } else {
                      this.comments[found] = newItem;
                    }

                  } else {

                    this.comments.push(newItem);

                  }

                }

  						}
              this.comments = this.catSorter(this.comments);

  					}

  				}

  			}

    	}

    });

    this.subscriptions.userVote = this.socket.on('user-vote').subscribe(val=>{

    	if(val.ok===true) {

    		if(val.item) {

    			if(val.item === this.item.stub){

    				this.item.vote = val.vote;
    				this.item.votes = val.votes;


    			}

    		}

    		if(val.comment) {

          this.setCommentVote(val);

    		}

    	}

    });

    this.subscriptions.userSave = this.socket.on('user-save').subscribe(val=>{

      if(val.ok === true){

        if(val.item){

          if(val.item === this.item.stub){

            this.item.saved = val.save;

          }

        }

        if(val.comment){

          this.setCommentSave(val);

        }

      }

    });

    this.subscriptions.userComment = this.socket.on('user-comment').subscribe(val=>{

      console.log(val);

      if(val.ok === true) {

        if(val.row.parent === null){
          let found = this.comments.findIndex(x=>x.stub === val.row.stub);
          if(found > -1){

            this.comments[found] = val.row;

          } else {

            this.comments.push(val.row);

          }

        } else {

          this.setReplies(val.row.parent,val.row);

        }

      }

    });

    this.subscriptions.category = this.category.subscribe(val=>{

	    this.socket.emit('items',{item:this.navParams.get('item'),_crc:this.crc});

    });

  }

  private setReplies(stub,comment,replies?){

    if(!replies){
      
      replies = this.comments;
    
    }
    if(replies.length > 0){

      let found = replies.findIndex(x=>x.stub === stub);
      if(found > -1){

        if(!replies[found].replies) replies[found].replies = [];
        
        let findReply = replies[found].replies.findIndex(y=>y.stub === comment.stub);
        if(findReply > -1){

          replies[found].replies[findReply] = comment;
        
        } else {
        
          replies[found].replies.push(comment);  
        
        }
        replies[found]._toggle = true;
        replies[found].replies = this.catSorter(replies[found].replies);

      } else {

        replies.forEach(reply=>{

          if(reply.replies){

            this.setReplies(stub,comment,reply.replies);

          } else {

            reply.replies = [];

          }

        });

      }

    }

  }

  private setCommentVote(vote,comments?){

    if(!comments){

        comments = this.comments;

    }

    let foundIndexInComments = comments.findIndex(x=>x.stub === vote.comment);
    if(foundIndexInComments > -1){

      comments[foundIndexInComments].vote = vote.vote;
      comments[foundIndexInComments].votes = vote.votes;
      comments = this.catSorter(comments);

    } else {

      if(comments.length>0){

        comments.forEach(comment=>{

          this.setCommentVote(vote,comment.replies);

        });

      }

    }

  }

  private setCommentSave(saveObj,comments?){

    if(!comments){

        comments = this.comments;

    }

    let foundIndexInComments = comments.findIndex(x=>x.stub === saveObj.comment);
    if(foundIndexInComments > -1){

      comments[foundIndexInComments].isSaved = saveObj.save;

    } else {

      if(comments.length>0){

        comments.forEach(comment=>{

          this.setCommentSave(saveObj,comment.replies);

        });

      }

    }

  }

  private catSorter(toSortArray){

    let cat = this.category.getValue();

    switch(cat){
      case 'likes':
        toSortArray = toSortArray.sort(this.fieldSorter(['-votes','-dt']));  
        break;
      case 'dislikes':
        toSortArray = toSortArray.sort(this.fieldSorter(['votes','-dt']));  
        break;
      case 'new':
        toSortArray = toSortArray.sort(this.fieldSorter(['-dt']));  
        break;
      case 'old':
        toSortArray = toSortArray.sort(this.fieldSorter(['dt']));  
        break;                
    }
    return toSortArray;

  }

  private fieldSorter(fields) {

    var dir = [], i, l = fields.length;
    fields = fields.map(function(o, i) {
      if (o[0] === "-") {
        dir[i] = -1;
        o = o.substring(1);
      } else {
        dir[i] = 1;
      }
      return o;

    });

      return function (a, b) {
        for (i = 0; i < l; i++) {
          var o = fields[i];
          if (a[o] > b[o]) return dir[i];
          if (a[o] < b[o]) return -(dir[i]);
        }
        return 0;
      };

  }

  ionViewWillLeave(){
  
    for(let subKey in this.subscriptions) {

      if(this.subscriptions[subKey] !== null){
        this.subscriptions[subKey].unsubscribe();
      }

    }
  
  }

  setCategory(category){

	  this.comments = [];
  	this.category.next(category._value);

  }

  commentBoxTrigger(evt){

    let popover = this.popCtrl.create(PopoverCommentBoxPage,{item:this.item.stub},{cssClass: 'comment-popover'});
    popover.present({
      ev: evt
    });

  }

  refresh(refresher:Refresher) {

    this.refresher = refresher;
    this.socket.emit('comments',{
      category:this.category.getValue(),
      _crc:this.crc,
      item:this.item.stub,  
    });

  }

  isLoggedIn() {

  	return this.data.getUser() !== null ? true : false;

  }

  getCommentsFrom(evt){

    if(this.comments.length > 0){

      let lastItem = this.comments[this.comments.length-1];
      if(lastItem){

      	let emit = {
          category:this.category.getValue(),
          from:lastItem.stub,
          _crc:this.crc,
          item:this.item.stub,
        };

        this.socket.emit('comments',emit);

      }
      this.infiniteScroller = evt;

    }

  }

  requestReplies(stub){

    this.socket.emit('comments',{
      parent:stub,
      _crc:this.crc,
    });

  }

}
