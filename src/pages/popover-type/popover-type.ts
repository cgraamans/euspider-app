import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { ModelDataProvider } from '../../providers/model-data/model-data';

@IonicPage()
@Component({
  selector: 'page-popover-type',
  templateUrl: 'popover-type.html',
})
export class PopoverTypePage {

  constructor(public navCtrl: NavController, public navParams: NavParams, private data:ModelDataProvider, private viewCtrl:ViewController) {}

  setType(type:any) {

  	this.data.setSourceType(type);
  	this.viewCtrl.dismiss();

  }

}
