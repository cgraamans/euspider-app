import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PopoverTypePage } from './popover-type';

@NgModule({
  declarations: [
    PopoverTypePage,
  ],
  imports: [
    IonicPageModule.forChild(PopoverTypePage),
  ],
})
export class PopoverTypePageModule {}
