import { Component } from '@angular/core';
import { IonicPage, NavController, ViewController } from 'ionic-angular';
import { LoginPage } from '../login/login';
import { ServiceSocketProvider } from '../../providers/service-socket/service-socket';
import { ModelDataProvider } from '../../providers/model-data/model-data';

/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {

  credentials = {
  	name:'',
  	password:'',
  };

  constructor(public navCtrl: NavController, private socket:ServiceSocketProvider,private data:ModelDataProvider,private viewCtrl:ViewController) {

	this.data.getUserObservable().subscribe(val=>{

		if(val){

			this.viewCtrl.dismiss();

		}

	});

  }

  public login() {

    this.viewCtrl.dismiss();
  }

  register(){

  	if((this.credentials.name.length>3) && (this.credentials.password.length>3)) {

  		this.socket.emit('auth-register',this.credentials);
  		
  	}

  }

}
