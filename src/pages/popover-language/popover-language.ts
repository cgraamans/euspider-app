import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { ModelDataProvider } from '../../providers/model-data/model-data';

@IonicPage()
@Component({
  selector: 'page-popover-language',
  templateUrl: 'popover-language.html',
})
export class PopoverLanguagePage {

  constructor(public navCtrl: NavController, public navParams: NavParams, private data:ModelDataProvider, private viewCtrl:ViewController) {}

  setLang(id:string) {

  	this.data.setLanguage(id);
  	this.viewCtrl.dismiss();

  }

}
