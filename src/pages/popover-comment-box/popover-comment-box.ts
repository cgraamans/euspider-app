import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

import { CommentBoxPage } from '../comment-box/comment-box'

@IonicPage()
@Component({
  selector: 'page-popover-comment-box',
  templateUrl: 'popover-comment-box.html',
})
export class PopoverCommentBoxPage {

  params = {
    txt:null,
    item:null,
    comment:null,
    edit:null,
  };

  constructor(public navCtrl: NavController, public navParams: NavParams, private viewCtrl:ViewController ) {
  
    if(this.navParams.get('txt')){
      this.params.txt = this.navParams.get('txt');
    }

    if(this.navParams.get('item')){
      this.params.item = this.navParams.get('item');
    }

    
    if(this.navParams.get('comment')){
      this.params.comment = this.navParams.get('comment');
    }

    if(this.navParams.get('edit')){
      this.params.edit = this.navParams.get('edit');

    }

  }

  onSubmit(evt){

    if(evt>0){

      this.viewCtrl.dismiss();  
    
    }
    
  }

  expandToPage(){

    this.navCtrl.push(CommentBoxPage,this.params);
	  this.viewCtrl.dismiss();  

  }

  dismiss() {

    this.viewCtrl.dismiss();

  }

}
