import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SourcePage } from './source';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    SourcePage
  ],
  imports: [
    IonicPageModule.forChild(SourcePage),
    ComponentsModule
  ],
})
export class SourcePageModule {}
