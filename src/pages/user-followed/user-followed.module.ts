import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UserFollowedPage } from './user-followed';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    UserFollowedPage,
  ],
  imports: [
    IonicPageModule.forChild(UserFollowedPage),
    ComponentsModule
  ],
})
export class UserFollowedPageModule {}
